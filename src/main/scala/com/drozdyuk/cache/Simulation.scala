package com.drozdyuk.cache

import akka.actor.ActorSystem
import akka.util.Timeout

import com.drozdyuk.feedback.Component.Work
import com.drozdyuk.feedback.{Identity, ClosedLoop, PidController}
import akka.pattern.ask
import scala.concurrent.duration._
import scala.concurrent.Await

/**
 * Created by Andriy Drozdyuk on 29-Jun-14.
 */
object Simulation {
  def main(args:Array[String]) {
    val system = ActorSystem("Simulation")
    implicit val timeout = Timeout(3.seconds)
    implicit val DT = 1f

    val p = system.actorOf(SmoothedCache.props(0, demand, 100))
    val c = system.actorOf(PidController.props(100, 250))

    system.actorOf(ClosedLoop.props(setpoint, c, p, 10000))


  /*  val identity = system.actorOf(Identity.props, "Identity")
    val result = Await.result(identity.ask(Work(1.0f)),1.second).asInstanceOf[Work]
    val cache = system.actorOf(Cache.props(100, demand), "Cache")
    val result2 = Await.result(cache.ask(Work(result.u)), 1.second).asInstanceOf[Work]
    val smoothedCache = system.actorOf(SmoothedCache.props(100, demand, 10), "SmoothedCache")
    val result3 = Await.result(smoothedCache.ask(Work(result2.u)), 1.second).asInstanceOf[Work]
    val pidController = system.actorOf(PidController.props(100, 250), "PidController")
    val result4 = Await.result(pidController.ask(Work(result3.u)), 1.second).asInstanceOf[Work]
    println(result)
    println(result2)
    println(result3)
    println(s"Result 4 = $result4")*/


  }
  def demand(t: Int) =  (math.random*15).toInt
  def setpoint(t: Int): Float = 0.7f



}
