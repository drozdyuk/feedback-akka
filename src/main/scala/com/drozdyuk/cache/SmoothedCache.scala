package com.drozdyuk.cache

import akka.actor.{Props, Actor}
import akka.actor.Actor.Receive
import akka.pattern.pipe
import akka.util.Timeout
import com.drozdyuk.feedback.Component.Work
import com.drozdyuk.feedback.FixedFilter
import akka.pattern.ask
import scala.concurrent.duration._
import scala.concurrent.Future

/**
 *
 * @param size The initial size of the cache.
 * @param demand Demand function that takes time parameter and returns the item to retrieve from cache.
 * @param avg Number of trailing requests that are averaged to form the hit rate.
 * @tparam A Type of the item stored in cache.
 */
class SmoothedCache[A](size:Int, demand:Int => A, avg: Int) extends Actor {
  val cache = context.actorOf(Cache.props(size, demand), "Cache")
  val f = context.actorOf(FixedFilter.props(avg), "FixedFilter")
  implicit val timeout = Timeout(1.second)
  implicit val ex = context.dispatcher

  override def receive: Receive = {
    case Work(u) => {
      val future = for {
        y <- (cache ? Work(u))
        result <- f ? y
      } yield result

      future pipeTo sender
    }
  }
}

object SmoothedCache {
  def props[A](size: Int, demand: Int => A, avg: Int): Props = Props(new SmoothedCache(size, demand, avg))
}
