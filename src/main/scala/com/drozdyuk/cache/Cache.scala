package com.drozdyuk.cache

import akka.actor.{Props, Actor}
import akka.actor.Actor.Receive
import com.drozdyuk.feedback.Component.Work

/**
 * Cache.
 * @param size
 * @param demand Reference function modeling the "demand" of item requests.
 * @tparam A Type of items stored in cache.
 */
class Cache[A](var size: Int, demand: Int => A) extends Actor {
  /** Internal time counter */
  var t: Int = 0

  /** Value is the last accessed time. */
  var cache: Map[A, Int] = Map.empty

  override def receive: Receive = {
    case Work(u) => {
      t += 1

      size = math.max(0, u.toInt) // non-negative integer

      val i = demand(t) // the "requested item"

      if (cache contains i) {
        cache = cache updated(i, t)
        sender ! Work(1)
      }
      else {
        if (cache.keys.size >= size) {
          // must make room
          // num of elems to delete
          val m = 1 + cache.keys.size - size
          // delete m oldest items
          cache = cache.toList.sortBy(_._2).drop(m).toMap
        }
        cache = cache + (i -> t) // insert into cache
        sender ! Work(0)
      }
    }
  }
}

object Cache {
  def props[A](size: Int, demand: Int => A): Props = Props(new Cache(size, demand))
}
