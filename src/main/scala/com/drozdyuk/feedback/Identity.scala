package com.drozdyuk.feedback

import akka.actor.{Props, Actor}
import com.drozdyuk.feedback.Component.Work

/** Simply reproduces the input as its output.
  * Used as a default argument to other components.
 * Created by Andriy Drozdyuk on 29-Jun-14.
 */
class Identity extends Actor {
  def receive = {
    case Work(u) => sender ! Work(u)
  }
}

object Identity {
  def props: Props = Props(new Identity())
}
