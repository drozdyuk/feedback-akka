package com.drozdyuk.feedback

import akka.actor.Actor
import com.drozdyuk.feedback.Component.Work

object Component {
  case class Work(u: Float)
}
