package com.drozdyuk.feedback

import akka.actor.{Props, ActorRef, Actor}
import akka.util.Timeout
import com.drozdyuk.feedback.Component.Work
import akka.pattern.ask
import scala.concurrent.Await
import scala.concurrent.duration._

/** Convenience function for implementing a closed loop.
 * Connects the components together and simply outputs the results
 * to the standard output.
 * Created by Andriy Drozdyuk on 29-Jun-14.
 *
 * @param setpoint function to provide a setpoint
 * @param controller the controller
 * @param plant plant (or process) instance
 * @param tm maximum number of simulation time steps
 * @param inverted if true inverts the tracking error (i.e. e -> -e) before it is passed to the controller
 * @param actuator optional actuator to insert between controller and the plant
 * @param returnfilter optional fiter to introduce on the return path from the plant to the controller (i.e. smoothing filter)
 * @param DT implicit time delta, that indicates what the time increments in simulation are
 */
class ClosedLoop(setpoint: Int => Float, controller: ActorRef,
                  plant: ActorRef,
                  tm: Int = 5000,
                  inverted: Boolean = false,
                  actuator: Props = Identity.props,
                  returnfilter: Props = Identity.props)(implicit DT:Float) extends Actor {

  val actuatorActor = context.actorOf(actuator, "Actuator")
  val returnFilterActor = context.actorOf(returnfilter, "ReturnFilter")

  implicit val timeout = Timeout(3.second)

  case object Start

  self ! Start

  def receive = {
    case Start => {
      var z: Float = 0f
      for (t <- 0 until tm) {
        val r = setpoint(t)
        var e = r - z
        if (inverted) e = -e
        val u = Await.result(controller ? Work(e), 3.seconds).asInstanceOf[Work].u
        val v = Await.result(actuatorActor ? Work(u), 3.seconds).asInstanceOf[Work].u
        val y = Await.result(plant ? Work(v), 3.seconds).asInstanceOf[Work].u
        z = Await.result(returnFilterActor ? Work(y), 3.seconds).asInstanceOf[Work].u

        println(s"$t, ${t * DT}, $r, $e, $u, $v, $y, $z")
      }
    }
  }
}

object ClosedLoop {

  /** Create a closed loop with a given components.
    *
    * @param setpoint function to provide a setpoint
    * @param controller the controller
    * @param plant plant (or process) instance
    * @param tm maximum number of simulation time steps
    * @param inverted if true inverts the tracking error (i.e. e -> -e) before it is passed to the controller
    * @param actuator optional actuator to insert between controller and the plant
    * @param returnfilter optional fiter to introduce on the return path from the plant to the controller (i.e. smoothing filter)
    * @param DT implicit time delta, that indicates what the time increments in simulation are
    * @return Props for the ClosedLoop actor.
    */
  def props(setpoint: Int => Float, controller: ActorRef,
            plant: ActorRef,
            tm: Int = 5000,
            inverted: Boolean = false,
            actuator: Props = Identity.props,
            returnfilter: Props = Identity.props)(implicit DT: Float): Props = {
    Props(new ClosedLoop(setpoint, controller, plant, tm, inverted, actuator, returnfilter))
  }
}
