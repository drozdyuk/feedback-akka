package com.drozdyuk.feedback

import akka.actor.{Props, Actor}
import akka.actor.Actor.Receive
import com.drozdyuk.feedback.Component.Work

import scala.collection.immutable.Queue

/**
 * Calculates unweighted average over it's last n inputs.
 * @param n The number of inputs to average over.
 */
class FixedFilter(n: Int) extends Actor {
  var data: Queue[Float] = Queue.empty

  override def receive: Receive = {
    case Work(u: Float) => {
      data = data.enqueue(u)
      if (data.length > n) {
        data = data.dequeue._2
      }

      sender ! Work(data.sum/data.length)
    }
  }
}

object FixedFilter {
  /**
   * Creates a new fixed filter actor.
   * @param n Number of inputs to average over.
   * @return Props with the next fixed filter actor.
   */
  def props(n: Int): Props = Props(new FixedFilter(n))
}
