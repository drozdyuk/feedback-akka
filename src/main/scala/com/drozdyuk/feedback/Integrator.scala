package com.drozdyuk.feedback

import akka.actor.{Actor}
import com.drozdyuk.feedback.Component.Work

/**
 * Created by Andriy Drozdyuk on 29-Jun-14.
 */
class Integrator(implicit DT: Float) extends Actor {
  var sum: Float = 0

  def integrate(x: Float): Float = {
    sum += x
    DT * sum
  }

  def receive = {
    case Work(u) => sender ! Work(integrate(u))
  }
}




