package com.drozdyuk.feedback

import akka.actor.{Props, Actor}
import akka.actor.Actor.Receive
import com.drozdyuk.feedback.Component.Work

/**
 * Standard PID controller.
 * @param kp Proportional gain.
 * @param ki Integral gain.
 * @param kd Derivative gain. Defaults to zero.
 * Created by Andriy Drozdyuk on 29-Jun-14.
 */
class PidController(var kp:Float, var ki:Float, var kd:Float = 0)(implicit DT:Float) extends Actor {
  var i: Float = 0
  var d: Float = 0
  var prev: Float = 0

  override def receive: Receive = {
    case Work(e) => {
      i += DT*e
      d = (e - prev)/DT
      prev = e
      sender ! Work(kp*e + ki*i + kd*d)
    }
  }
}

object PidController {
  def props(kp: Float, ki: Float, kd: Float = 0)(implicit DI: Float):Props = {
    Props(new PidController(kp, ki, kd))
  }
}
