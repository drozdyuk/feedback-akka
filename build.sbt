name := """minimal-scala"""

version := "1.0"

// Versions of core libraries
val versions:Map[String, String] = Map(
  "akka" -> "2.3.3",
  "scala" -> "2.11.1",
  "scalaTest" -> "2.1.6"
)

val dependencies = Seq(
  "com.typesafe.akka" %% "akka-actor" % versions("akka"),
  "org.scalatest" %% "scalatest" % versions("scalaTest"),
  "nl.grons" %% "metrics-scala" % "3.2.0_a2.3"
)

lazy val buildSettings = Defaults.coreDefaultSettings ++ Seq(
  organization := "com.drozdyuk",
  version := "1.0",
  scalaVersion := versions("scala"),
  autoCompilerPlugins := true
)


// Project definition!
lazy val project = Project(
  id = "feedback-akka",
  base=file("."),
  settings=buildSettings ++ Seq(libraryDependencies ++= dependencies)
)
